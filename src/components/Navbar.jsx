import logo from './../assets/react.svg'

export default function Navbar() {
    return (
        <div className="navbar">
            <div className="logo">
                <img src={logo} alt="The React logo is a representation of an atom" />
                <span>ReactFacts</span>
            </div>
            <div className="nav-title">React Course - Project 1</div>
        </div>
    )
}